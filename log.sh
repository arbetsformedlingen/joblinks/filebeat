#!/usr/bin/env bash

echo "INFO: this step is temporarily disabled while sorting out changes to log format"
exit

logdir="$1"
secretsdir="$2"
conf="$3"
elkhost="${4:-elk-test.jobtechdev.se:5044}"


if [ -z "$logdir" ]; then echo "**** supply logdir" >&2; exit 1; fi
if [ ! -f "$conf" ]; then echo "**** cannot find conf $conf" >&2; exit 1; fi

# Run the application
CONF=$(mktemp)
sed "s|\$LOGFILEPATTERN|$logdir/**/err|" < "$conf" |\
    sed "s|\$ELKHOST|$elkhost|" |\
    sed "s|\$SECRETSDIR|$secretsdir|" > "$CONF"

trap "rm -f $CONF" EXIT

filebeat -c "$CONF" run --once
