FROM docker.io/library/ubuntu:22.04

RUN mkdir /app

WORKDIR /app

COPY log.sh /app
COPY filebeat.yaml /app

RUN apt-get -y update \
    && apt-get -y install wget gnupg git \
    && wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add - \
    && apt-get -y install apt-transport-https \
    && echo "deb https://artifacts.elastic.co/packages/oss-8.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-8.x.list \
    && apt-get -y update \
    && apt-get -y install filebeat \
    && apt-get -y clean \
    && apt-get -y autoremove

ENTRYPOINT [ "bash", "/app/log.sh" ]
